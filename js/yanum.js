let menu = [
    {'nama':'Pendidikan','gambar':"yu-didik.png"},
    {'nama':'Keamanan','gambar':"yu-aman.png"},
    {'nama':'Kesehatan','gambar':"yu-waras.png"},
    {'nama':'Pemerintahan','gambar':"yu-prentah.png"},
    {'nama':'Keuangan','gambar':"yu-duwit.png"},
    {'nama':'Keagamaan','gambar':"yu-badah.png"}
];

$( document ).ready( function(){
    $(".menu-holder div").remove();
    $.each(menu , function(i,data){
        $(".menu-holder").append(`
        <div class='menu-box'>
          <img src='./img/${data.gambar}' class='menu-image'>
          <div class='menu-title py-1'><a class='text-light' href='sub-pubsvc.html'>${data.nama}</a></div>
        </div>
        `);
    })
})